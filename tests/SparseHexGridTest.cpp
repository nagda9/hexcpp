// copyright

#include <gtest/gtest.h>

#include "Hex.hpp"
#include "SparseHexGrid.hpp"

TEST(SparseHexGridTest, testInsertionAndAccess) {
    SparseHexGrid<int> m;

    m[{0, 0}] = 10;
    m[{-1, 1}] = 12;
    m[{0, 2}] = 2;

    EXPECT_TRUE(m.contains(Hex(0, 0)));
    EXPECT_TRUE(m.contains(Hex(-1, 1)));
    EXPECT_TRUE(m.contains(Hex(0, 2)));
    EXPECT_EQ(m.at(Hex(0, 2)), (m.at(Hex(-1, 1)) - m[Hex(0, 0)]));

    m[Hex(0, 2)] = 4;
    EXPECT_EQ(m.storageSize(), 3);
}

TEST(SparseHexGridTest, testEraseAndEmpty) {
    SparseHexGrid<int> m;

    m[Hex(0, 0)] = 1;
    m[Hex(0, 1)] = 1;
    m.erase(Hex(0, 1));

    EXPECT_FALSE(m.contains(Hex(0, 1)));
    EXPECT_EQ(m.storageSize(), 1);
    EXPECT_FALSE(m.rowEmpty(0));
    EXPECT_FALSE(m.columnEmpty(0));
    EXPECT_TRUE(m.columnEmpty(1));
    EXPECT_FALSE(m.empty());

    m.erase(Hex(0, 0));
    EXPECT_EQ(m.storageSize(), 0);
    EXPECT_TRUE(m.columnEmpty(0));
    EXPECT_TRUE(m.rowEmpty(0));
    EXPECT_TRUE(m.empty());

    EXPECT_NO_THROW(m.erase(Hex(-5, -5)));
}

TEST(SparseHexGridTest, testIterator) {
    SparseHexGrid<int> grid;

    grid[Hex(0, 0)] = 1;
    grid[Hex(-1, 1)] = 2;
    grid[Hex(0, 2)] = 3;

    auto it = grid.begin();
    std::vector<int> intVec1{};
    for (size_t i = 0; i < grid.storageSize(); i++) {
        intVec1.push_back(*it);
        ++it;
    }
    it = grid.begin();
    std::vector<int> intVec2{};
    for (size_t i = 0; i < grid.storageSize(); i++) {
        intVec2.push_back(*it);
        it++;
    }
    std::vector<int> intVec3{};
    for (const auto& g : grid) {
        intVec3.push_back(g);
    }
    EXPECT_EQ(intVec1, intVec2);
    EXPECT_EQ(intVec1, intVec3);
    EXPECT_EQ(intVec1, (std::vector<int>{2, 3, 1}));

    EXPECT_EQ(grid.end(), std::next(grid.begin(), grid.storageSize()));
}

TEST(SparseHexGridTest, testConstructorsAndAssignment) {
    SparseHexGrid<int> n;
    SparseHexGrid<int> nCopy{n};  // call copy constructor
    EXPECT_TRUE(n.empty());
    EXPECT_EQ(n, nCopy);

    n[Hex(0, 0)] = 1;
    n[Hex(1, 0)] = 2;
    n[Hex(3, 0)] = 3;

    SparseHexGrid<int> nCopy2{n};  // call copy constructor
    nCopy = n;                     // call copy assignment
    EXPECT_EQ(n, nCopy);
    EXPECT_EQ(nCopy, nCopy2);

    SparseHexGrid<int> m;
    m[Hex(0, 0)] = 1;
    m[Hex(1, 0)] = 2;

    n.erase(Hex(3, 0));
    EXPECT_EQ(n, m);

    nCopy = std::move(n);  // move assignment
    EXPECT_EQ(n, nCopy);

    SparseHexGrid<int> mCopy{std::move(m)};  // move constructor
    EXPECT_EQ(mCopy, nCopy);
}
