// copyright

#include <gtest/gtest.h>

#include "Hex.hpp"

TEST(HexTest, testHexArithmetic) {
    EXPECT_EQ(Hex(4, -10, 6), hex_add(Hex(1, -3, 2), Hex(3, -7, 4)));
}

TEST(HexTest, testHexSubtract) {
    EXPECT_EQ(Hex(-2, 4, -2), hex_subtract(Hex(1, -3, 2), Hex(3, -7, 4)));
}

TEST(HexTest, testHexDirection) {
    EXPECT_EQ(Hex(0, -1, 1), hex_direction(2));
}

TEST(HexTest, testHexNeighbor) {
    EXPECT_EQ(Hex(1, -3, 2), hex_neighbor(Hex(1, -2, 1), 2));
}

TEST(HexTest, testHexDiagonal) {
    EXPECT_EQ(Hex(-1, -1, 2), hex_diagonal_neighbor(Hex(1, -2, 1), 3));
}

TEST(HexTest, testHexDistance) {
    EXPECT_EQ(7, hex_distance(Hex(3, -7, 4), Hex(0, 0, 0)));
}

TEST(HexTest, testHexRotateRight) {
    EXPECT_EQ(hex_rotate_right(Hex(1, -3, 2)), Hex(3, -2, -1));
}

TEST(HexTest, testHexRotateLeft) {
    EXPECT_EQ(hex_rotate_left(Hex(1, -3, 2)), Hex(-2, -1, 3));
}

TEST(HexTest, testHexRound) {
    FractionalHex a = FractionalHex(0.0, 0.0, 0.0);
    FractionalHex b = FractionalHex(1.0, -1.0, 0.0);
    FractionalHex c = FractionalHex(0.0, -1.0, 1.0);
    EXPECT_EQ(Hex(5, -10, 5), hex_round(hex_lerp(FractionalHex(0.0, 0.0, 0.0), FractionalHex(10.0, -20.0, 10.0), 0.5)));
    EXPECT_EQ(hex_round(a), hex_round(hex_lerp(a, b, 0.499)));
    EXPECT_EQ(hex_round(b), hex_round(hex_lerp(a, b, 0.501)));
    EXPECT_EQ(hex_round(a),
              hex_round(FractionalHex(a.getQ() * 0.4 + b.getQ() * 0.3 + c.getQ() * 0.3,
                                      a.getR() * 0.4 + b.getR() * 0.3 + c.getR() * 0.3,
                                      a.getS() * 0.4 + b.getS() * 0.3 + c.getS() * 0.3)));
    EXPECT_EQ(hex_round(c),
              hex_round(FractionalHex(a.getQ() * 0.3 + b.getQ() * 0.3 + c.getQ() * 0.4,
                                      a.getR() * 0.3 + b.getR() * 0.3 + c.getR() * 0.4,
                                      a.getS() * 0.3 + b.getS() * 0.3 + c.getS() * 0.4)));
}

TEST(HexTest, testHexLinedraw) {
    EXPECT_EQ(
        (std::vector<Hex>{Hex(0, 0, 0), Hex(0, -1, 1), Hex(0, -2, 2), Hex(1, -3, 2), Hex(1, -4, 3), Hex(1, -5, 4)}),
        hex_linedraw(Hex(0, 0, 0), Hex(1, -5, 4)));
}

TEST(HexTest, testLayout) {
    Hex h = Hex(3, 4, -7);
    Layout flat = Layout(layout_flat, Point(10.0, 15.0), Point(35.0, 71.0));
    EXPECT_EQ(h, hex_round(pixel_to_hex(flat, hex_to_pixel(flat, h))));
    Layout pointy = Layout(layout_pointy, Point(10.0, 15.0), Point(35.0, 71.0));
    EXPECT_EQ(h, hex_round(pixel_to_hex(pointy, hex_to_pixel(pointy, h))));
}

TEST(HexTest, testOffsetRoundtrip) {
    Hex a = Hex(3, 4, -7);
    OffsetCoord b = OffsetCoord(1, -3);
    EXPECT_EQ(a, qoffset_to_cube(EVEN, qoffset_from_cube(EVEN, a)));
    EXPECT_EQ(b, qoffset_from_cube(EVEN, qoffset_to_cube(EVEN, b)));
    EXPECT_EQ(a, qoffset_to_cube(ODD, qoffset_from_cube(ODD, a)));
    EXPECT_EQ(b, qoffset_from_cube(ODD, qoffset_to_cube(ODD, b)));
    EXPECT_EQ(a, roffset_to_cube(EVEN, roffset_from_cube(EVEN, a)));
    EXPECT_EQ(b, roffset_from_cube(EVEN, roffset_to_cube(EVEN, b)));
    EXPECT_EQ(a, roffset_to_cube(ODD, roffset_from_cube(ODD, a)));
    EXPECT_EQ(b, roffset_from_cube(ODD, roffset_to_cube(ODD, b)));
}

TEST(HexTest, testOffsetFromCube) {
    EXPECT_EQ(OffsetCoord(1, 3), qoffset_from_cube(EVEN, Hex(1, 2, -3)));
    EXPECT_EQ(OffsetCoord(1, 2), qoffset_from_cube(ODD, Hex(1, 2, -3)));
}

TEST(HexTest, testOffsetToCube) {
    EXPECT_EQ(Hex(1, 2, -3), qoffset_to_cube(EVEN, OffsetCoord(1, 3)));
    EXPECT_EQ(Hex(1, 2, -3), qoffset_to_cube(ODD, OffsetCoord(1, 2)));
}

TEST(HexTest, testDoubledRoundtrip) {
    Hex a = Hex(3, 4, -7);
    DoubledCoord b = DoubledCoord(1, -3);
    EXPECT_EQ(a, qdoubled_to_cube(qdoubled_from_cube(a)));
    EXPECT_EQ(b, qdoubled_from_cube(qdoubled_to_cube(b)));
    EXPECT_EQ(a, rdoubled_to_cube(rdoubled_from_cube(a)));
    EXPECT_EQ(b, rdoubled_from_cube(rdoubled_to_cube(b)));
}

TEST(HexTest, testDoubledFromCube) {
    EXPECT_EQ(DoubledCoord(1, 5), qdoubled_from_cube(Hex(1, 2, -3)));
    EXPECT_EQ(DoubledCoord(4, 2), rdoubled_from_cube(Hex(1, 2, -3)));
}

TEST(HexTest, testDoubledToCube) {
    EXPECT_EQ(Hex(1, 2, -3), qdoubled_to_cube(DoubledCoord(1, 5)));
    EXPECT_EQ(Hex(1, 2, -3), rdoubled_to_cube(DoubledCoord(4, 2)));
}

TEST(HexTest, testHexGridSize) {
    EXPECT_EQ(hex_grid_size(0), 1);
    EXPECT_EQ(hex_grid_size(1), 7);
    EXPECT_EQ(hex_grid_size(2), 19);
    EXPECT_EQ(hex_grid_size(3), 37);
    EXPECT_EQ(hex_grid_size(4), 61);
}
