if (TARGET hex)
    return()
endif()

get_filename_component(_IMPORT_PREFIX "${CMAKE_CURRENT_LIST_FILE}" PATH)
get_filename_component(_IMPORT_PREFIX "${_IMPORT_PREFIX}" PATH)
if (_IMPORT_PREFIX STREQUAL "/")
  set(_IMPORT_PREFIX "")
endif()

set(HEX_INCLUDE_DIRS ${_IMPORT_PREFIX})

add_library(hex INTERFACE IMPORTED)
set_target_properties(hex PROPERTIES INTERFACE_INCLUDE_DIRECTORIES ${HEX_INCLUDE_DIRS})

mark_as_advanced(hex_DIR)
set(_IMPORT_PREFIX)