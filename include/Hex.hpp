// copyright

#pragma once

#include <cassert>

#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <iterator>
#include <tuple>
#include <utility>
#include <vector>

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

struct Point {
    const double x;
    const double y;
    Point(double x_, double y_)
      : x(x_)
      , y(y_) {}
};

class Hex {
 private:
    int q;
    int r;
    int s;

 public:
    Hex(int q_, int r_)
      : q(q_)
      , r(r_)
      , s(-q_ - r_) {}

    Hex(int q_, int r_, int s_)
      : q(q_)
      , r(r_)
      , s(s_) {
        assert(q + r + s == 0);
    }

    int getQ() const {
        return q;
    }

    int getR() const {
        return r;
    }

    int getS() const {
        return s;
    }

    void setHex(int q_, int r_) {
        q = q_;
        r = r_;
        s = -q_ - r_;
    }

    void setHex(int q_, int r_, int s_) {
        q = q_;
        r = r_, s = s_;
    }

    struct HashFunction {
        size_t operator()(const Hex& hex) const noexcept{
            return static_cast<size_t>(hex.getQ() * 31 + hex.getR());
        }
    };

    bool operator==(Hex rhs) const {
        return q == rhs.getQ() && r == rhs.getR() && s == rhs.getS();
    }

    bool operator!=(Hex rhs) const {
        return !(*this == rhs);
    }

    bool operator<(const Hex& rhs) const {
        int rhsQ = rhs.getQ();
        int rhsR = rhs.getR();
        return std::tie(q, r) < std::tie(rhsQ, rhsR);
    }

    friend std::ostream& operator<<(std::ostream& os, Hex hex) {
        os << "(" << hex.q << ", " << hex.r << ", " << hex.s << ")";
        return os;
    }
};

class FractionalHex {
 private:
    double q;
    double r;
    double s;

 public:
    FractionalHex(double q_, double r_)
      : q(q_)
      , r(r_)
      , s(-q_ - r_) {}

    FractionalHex(double q_, double r_, double s_)
      : q(q_)
      , r(r_)
      , s(s_) {
        assert(round(q + r + s) == 0);
    }

    double getQ() const {
        return q;
    }

    double getR() const {
        return r;
    }

    double getS() const {
        return s;
    }

    void setHex(int q_, int r_) {
        q = q_;
        r = r_;
        s = -q_ - r_;
    }

    void setHex(int q_, int r_, int s_) {
        q = q_;
        r = r_, s = s_;
    }

    friend std::ostream& operator<<(std::ostream& os, FractionalHex hex) {
        os << "(" << hex.q << ", " << hex.r << ", " << hex.s << ")";
        return os;
    }
};

struct OffsetCoord {
 private:
    int col;
    int row;

 public:
    OffsetCoord(int col_, int row_)
      : col(col_)
      , row(row_) {}

    int getCol() const {
        return col;
    }

    int getRow() const {
        return row;
    }

    void set(int col_, int row_) {
        col = col_;
        row = row_;
    }

    bool operator==(OffsetCoord rhs) const {
        return col == rhs.getCol() && row == rhs.getRow();
    }

    friend std::ostream& operator<<(std::ostream& os, OffsetCoord hex) {
        os << "(" << hex.col << ", " << hex.row << ")";
        return os;
    }
};

class DoubledCoord {
 private:
    int col;
    int row;

 public:
    DoubledCoord(int col_, int row_)
      : col(col_)
      , row(row_) {}

    int getCol() const {
        return col;
    }

    int getRow() const {
        return row;
    }

    void set(int col_, int row_) {
        col = col_;
        row = row_;
    }

    bool operator==(DoubledCoord rhs) const {
        return col == rhs.getCol() && row == rhs.getRow();
    }

    friend std::ostream& operator<<(std::ostream& os, DoubledCoord hex) {
        os << "(" << hex.col << ", " << hex.row << ")";
        return os;
    }
};

struct Orientation {
    const double f0;
    const double f1;
    const double f2;
    const double f3;
    const double b0;
    const double b1;
    const double b2;
    const double b3;
    const double start_angle;
    Orientation(double f0_,
                double f1_,
                double f2_,
                double f3_,
                double b0_,
                double b1_,
                double b2_,
                double b3_,
                double start_angle_)
      : f0(f0_)
      , f1(f1_)
      , f2(f2_)
      , f3(f3_)
      , b0(b0_)
      , b1(b1_)
      , b2(b2_)
      , b3(b3_)
      , start_angle(start_angle_) {}
};

struct Layout {
    const Orientation orientation;
    const Point size;
    const Point origin;
    Layout(Orientation orientation_, Point size_, Point origin_)
      : orientation(orientation_)
      , size(size_)
      , origin(origin_) {}
};

// Forward declarations

inline Hex hex_add(Hex lhs, Hex rhs) {
    return Hex(lhs.getQ() + rhs.getQ(), lhs.getR() + rhs.getR(), lhs.getS() + rhs.getS());
}

inline Hex hex_subtract(Hex lhs, Hex rhs) {
    return Hex(lhs.getQ() - rhs.getQ(), lhs.getR() - rhs.getR(), lhs.getS() - rhs.getS());
}

inline Hex hex_scale(Hex lhs, int k) {
    return Hex(lhs.getQ() * k, lhs.getR() * k, lhs.getS() * k);
}

inline Hex hex_rotate_left(Hex lhs) {
    return Hex(-lhs.getS(), -lhs.getQ(), -lhs.getR());
}

inline Hex hex_rotate_right(Hex lhs) {
    return Hex(-lhs.getR(), -lhs.getS(), -lhs.getQ());
}

const std::vector<Hex> hex_directions =
    {Hex(1, 0, -1), Hex(1, -1, 0), Hex(0, -1, 1), Hex(-1, 0, 1), Hex(-1, 1, 0), Hex(0, 1, -1)};

inline Hex hex_direction(size_t direction) {
    return hex_directions[direction];
}

inline Hex hex_neighbor(Hex hex, size_t direction) {
    return hex_add(hex, hex_direction(direction));
}

const std::vector<Hex> hex_diagonals =
    {Hex(2, -1, -1), Hex(1, -2, 1), Hex(-1, -1, 2), Hex(-2, 1, 1), Hex(-1, 2, -1), Hex(1, 1, -2)};

inline Hex hex_diagonal_neighbor(Hex hex, size_t direction) {
    return hex_add(hex, hex_diagonals[direction]);
}

inline size_t hex_length(Hex hex) {
    return static_cast<size_t>((abs(hex.getQ()) + std::abs(hex.getR()) + std::abs(hex.getS())) / 2);
}

inline size_t hex_distance(Hex lhs, Hex rhs) {
    return hex_length(hex_subtract(lhs, rhs));
}

inline Hex hex_round(FractionalHex h) {
    int qi = static_cast<int>(round(h.getQ()));
    int ri = static_cast<int>(round(h.getR()));
    int si = static_cast<int>(round(h.getS()));
    double q_diff = std::abs(qi - h.getQ());
    double r_diff = std::abs(ri - h.getR());
    double s_diff = std::abs(si - h.getS());
    if (q_diff > r_diff && q_diff > s_diff) {
        qi = -ri - si;
    } else if (r_diff > s_diff) {
        ri = -qi - si;
    } else {
        si = -qi - ri;
    }
    return Hex(qi, ri, si);
}

inline FractionalHex hex_lerp(FractionalHex lhs, FractionalHex rhs, double t) {
    return FractionalHex(lhs.getQ() * (1.0 - t) + rhs.getQ() * t,
                         lhs.getR() * (1.0 - t) + rhs.getR() * t,
                         lhs.getS() * (1.0 - t) + rhs.getS() * t);
}

inline std::vector<Hex> hex_linedraw(Hex lhs, Hex rhs) {
    size_t N = hex_distance(lhs, rhs);
    FractionalHex lhs_nudge = FractionalHex(lhs.getQ() + 1e-06, lhs.getR() + 1e-06, lhs.getS() - 2e-06);
    FractionalHex rhs_nudge = FractionalHex(rhs.getQ() + 1e-06, rhs.getR() + 1e-06, rhs.getS() - 2e-06);
    std::vector<Hex> results = {};
    double step = 1.0 / std::max(static_cast<int>(N), 1);
    for (size_t i = 0; i <= N; i++) {
        results.push_back(hex_round(hex_lerp(lhs_nudge, rhs_nudge, step * static_cast<double>(i))));
    }
    return results;
}

const int EVEN = 1;
const int ODD = -1;
inline OffsetCoord qoffset_from_cube(int offset, Hex h) {
    int col = h.getQ();
    int row = h.getR() + static_cast<int>((h.getQ() + offset * (h.getQ() & 1)) / 2);
    if (offset != EVEN && offset != ODD) {
        throw "offset must be EVEN (+1) or ODD (-1)";
    }
    return OffsetCoord(col, row);
}

inline Hex qoffset_to_cube(int offset, OffsetCoord h) {
    int q = h.getCol();
    int r = h.getRow() - static_cast<int>((h.getCol() + offset * (h.getCol() & 1)) / 2);
    int s = -q - r;
    if (offset != EVEN && offset != ODD) {
        throw "offset must be EVEN (+1) or ODD (-1)";
    }
    return Hex(q, r, s);
}

inline OffsetCoord roffset_from_cube(int offset, Hex h) {
    int col = h.getQ() + static_cast<int>((h.getR() + offset * (h.getR() & 1)) / 2);
    int row = h.getR();
    if (offset != EVEN && offset != ODD) {
        throw "offset must be EVEN (+1) or ODD (-1)";
    }
    return OffsetCoord(col, row);
}

inline Hex roffset_to_cube(int offset, OffsetCoord h) {
    int q = h.getCol() - static_cast<int>((h.getRow() + offset * (h.getRow() & 1)) / 2);
    int r = h.getRow();
    int s = -q - r;
    if (offset != EVEN && offset != ODD) {
        throw "offset must be EVEN (+1) or ODD (-1)";
    }
    return Hex(q, r, s);
}

inline DoubledCoord qdoubled_from_cube(Hex h) {
    int col = h.getQ();
    int row = 2 * h.getR() + h.getQ();
    return DoubledCoord(col, row);
}

inline Hex qdoubled_to_cube(DoubledCoord h) {
    int q = h.getCol();
    int r = static_cast<int>((h.getRow() - h.getCol()) / 2);
    int s = -q - r;
    return Hex(q, r, s);
}

inline DoubledCoord rdoubled_from_cube(Hex h) {
    int col = 2 * h.getQ() + h.getR();
    int row = h.getR();
    return DoubledCoord(col, row);
}

inline Hex rdoubled_to_cube(DoubledCoord h) {
    int q = static_cast<int>((h.getCol() - h.getRow()) / 2);
    int r = h.getRow();
    int s = -q - r;
    return Hex(q, r, s);
}

const Orientation layout_pointy =
    Orientation(sqrt(3.0), sqrt(3.0) / 2.0, 0.0, 3.0 / 2.0, sqrt(3.0) / 3.0, -1.0 / 3.0, 0.0, 2.0 / 3.0, 0.5);

const Orientation layout_flat =
    Orientation(3.0 / 2.0, 0.0, sqrt(3.0) / 2.0, sqrt(3.0), 2.0 / 3.0, 0.0, -1.0 / 3.0, sqrt(3.0) / 3.0, 0.0);

inline Point hex_to_pixel(Layout layout, Hex h) {
    Orientation M = layout.orientation;
    Point size = layout.size;
    Point origin = layout.origin;
    double x = (M.f0 * h.getQ() + M.f1 * h.getR()) * size.x;
    double y = (M.f2 * h.getQ() + M.f3 * h.getR()) * size.y;
    return Point(x + origin.x, y + origin.y);
}

inline Point hex_to_pixel_flipped(Layout layout, Hex h) {
    Orientation M = layout.orientation;
    Point size = layout.size;
    Point origin = layout.origin;
    double x = (M.f0 * h.getQ() + M.f1 * h.getR()) * size.x;
    double y = -(M.f2 * h.getQ() + M.f3 * h.getR()) * size.y;
    return Point(x + origin.x, y + origin.y);
}

inline FractionalHex pixel_to_hex(Layout layout, Point p) {
    Orientation M = layout.orientation;
    Point size = layout.size;
    Point origin = layout.origin;
    Point pt = Point((p.x - origin.x) / size.x, (p.y - origin.y) / size.y);
    double q = M.b0 * pt.x + M.b1 * pt.y;
    double r = M.b2 * pt.x + M.b3 * pt.y;
    return FractionalHex(q, r, -q - r);
}

inline Point hex_corner_offset(Layout layout, int corner) {
    Orientation M = layout.orientation;
    Point size = layout.size;
    double angle = 2.0 * M_PI * (M.start_angle - corner) / 6.0;
    return Point(size.x * cos(angle), size.y * sin(angle));
}

inline std::vector<Point> polygon_corners(Layout layout, Hex h) {
    std::vector<Point> corners = {};
    Point center = hex_to_pixel(layout, h);
    for (int i = 0; i < 6; i++) {
        Point offset = hex_corner_offset(layout, i);
        corners.push_back(Point(center.x + offset.x, center.y + offset.y));
    }
    return corners;
}

inline size_t hex_grid_size(const size_t radius) {
    return 1 + 6 * (radius * (radius + 1)) / 2;  // 1 + 6 * sum_{i=0}^{radius}(i)
}

inline bool operator==(std::vector<Hex> lhs, std::vector<Hex> rhs) {
    assert(lhs.size() == rhs.size());
    for (size_t i = 0; i < lhs.size(); i++) {
        if (lhs[i] != rhs[i]) {
            return false;
        }
    }
    return true;
}
