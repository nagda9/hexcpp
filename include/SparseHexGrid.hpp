// copyright

#pragma once

#include <vector>
#include <unordered_map>
#include <utility>
#include <memory>

#include "Hex.hpp"

template<typename T>
class SparseHexGridIterator {
 private:
    std::vector<std::shared_ptr<T>> nodes{};
    size_t current{0};

 public:
    // Iterator traits, previously from std::iterator.
    using value_type = T;
    using difference_type = size_t;
    using pointer = std::shared_ptr<T>;
    using reference = T&;
    using iterator_category = std::bidirectional_iterator_tag;

    SparseHexGridIterator() {
        nodes.push_back(nullptr);
    }

    SparseHexGridIterator(const std::unordered_map<int, std::unordered_map<int, std::shared_ptr<T>>>& mat,
                          size_t aCurrent)
      : current(aCurrent) {
        for (const auto& [n, row] : mat) {
            for (const auto [m, value] : row) {
                nodes.push_back(value);
            }
        }
        nodes.push_back(nullptr);
    }

    reference operator*() const {
        return *nodes[current];
    }

    // increase, then return
    SparseHexGridIterator& operator++() {
        ++current;
        return *this;
    }

    // return, then increase
    SparseHexGridIterator operator++(int) {
        SparseHexGridIterator tmp = *this;
        ++current;
        return tmp;
    }

    SparseHexGridIterator& operator--() {
        --current;
        return *this;
    }
    SparseHexGridIterator operator--(int) {
        SparseHexGridIterator tmp = *this;
        --current;
        return tmp;
    }

    bool operator==(const SparseHexGridIterator& rhs) const {
        return nodes[current] == rhs.nodes[rhs.current];
    }

    bool operator!=(const SparseHexGridIterator& rhs) const {
        return !(*this == rhs);
    }
};

template<typename T>
class SparseHexGrid {
 public:
    std::unordered_map<int, std::unordered_map<int, std::shared_ptr<T>>>
        mat{};  // dont access this directly if not necessary

    using iterator = SparseHexGridIterator<T>;
    using const_iterator = SparseHexGridIterator<const T>;

    iterator begin() const {
        return SparseHexGridIterator<T>(mat, 0);
    }

    iterator end() const {
        return SparseHexGridIterator<T>(mat, storageSize());
    }

    SparseHexGrid() = default;

    // copy constructor
    SparseHexGrid(const SparseHexGrid& src) {
        for (const auto& [n, row] : src.mat) {
            for (const auto& [m, value] : row) {
                (*this)[{n, m}] = *value;
            }
        }
    }

    // move constructor
    SparseHexGrid(SparseHexGrid&& src)  noexcept {
        src.swap(*this);
    }

    ~SparseHexGrid() = default;

    // copy assignment
    SparseHexGrid& operator=(const SparseHexGrid& rhs) {
        if (&rhs != this) {
            SparseHexGrid tmp{rhs};  // calls copy constructor
            tmp.swap(*this);
        }
        return *this;
    }

    // move assignment
    SparseHexGrid& operator=(SparseHexGrid&& rhs) {
        SparseHexGrid tmp{std::move(rhs)};  // calls move constructor
        tmp.swap(*this);
        return *this;
    }

    bool operator==(const SparseHexGrid& rhs) const {
        if (storageSize() == rhs.storageSize()) {
            for (const auto& [n, row] : rhs.mat) {
                for (const auto& [m, value] : row) {
                    if (!contains(Hex(n, m))) {
                        return false;
                    }
                    if (at(Hex(n, m)) != *value) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    bool operator!=(const SparseHexGrid& rhs) const {
        return !(*this == rhs);
    }

    void swap(SparseHexGrid& rhs) {
        std::swap(mat, rhs.mat);
    }

    void erase(const Hex& hex) {
        if (contains(hex)) {
            int q = hex.getQ();
            int r = hex.getR();
            mat.at(q).erase(r);

            if (mat.at(q).empty()) {
                mat.erase(q);
            }
        }
    }

    T at(const Hex& hex) const {
        return *mat.at(hex.getQ()).at(hex.getR());
    }

    T& operator[](const Hex& hex) {
        if (!contains(hex)) {
            mat[hex.getQ()][hex.getR()] = std::make_shared<T>();
        }
        return *mat[hex.getQ()][hex.getR()];
    }

    bool empty() {
        return mat.empty();
    }

    bool rowEmpty(int n) {
        if (mat.find(n) != mat.end()) {
            return false;
        }
        return true;
    }

    bool columnEmpty(int m) {
        for (const auto& [n, row] : mat) {
            if (row.find(m) != row.end()) {
                return false;
            }
        }
        return true;
    }

    [[nodiscard]] bool contains(const Hex& hex) const {
        if (mat.find(hex.getQ()) == mat.end())
            return false;

        if (mat.at(hex.getQ()).find(hex.getR()) == mat.at(hex.getQ()).end()) {
            return false;
        }
        return true;
    }

    [[nodiscard]] size_t storageSize() const {
        size_t count = 0;
        for (const auto& [n, row] : mat) {
            count += row.size();
        }
        return count;
    }

    void clear() {
        mat.clear();
    }

    friend std::ostream& operator<<(std::ostream& os, const SparseHexGrid& reader) {
        for (const auto& [n, row] : reader.mat) {
            for (const auto& [m, value] : row) {
                os << "\n(" << n << ", " << m << ") " << *value;
            }
        }
        return os;
    }
};
