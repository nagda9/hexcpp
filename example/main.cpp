// copyright

#include <iostream>

#include "Hex.hpp"
#include "SparseHexGrid.hpp"

int main() {
    std::cout << "Hex grid size with radius=1: " << hex_grid_size(1) << "\n";
}
